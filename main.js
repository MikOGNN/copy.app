const glasstron = require('glasstron');
const {app, globalShortcut, BrowserWindow} = require('electron');
const clipboard = require('electron-clipboard-extended');
const applescript = require("applescript");
const fs = require("fs");
const plist = require("plist");
var iconutil = require('iconutil');

class ClipboardItem {
	constructor(type, content, app) {
		this.time    = new Date();
		this.type    = type;
		this.content = content;
		this.app     = app;
	}
}

app.commandLine.appendSwitch("enable-transparent-visuals");
app.on('ready', () => {
	setTimeout(
		spawnWindow,
		process.platform == "linux" ? 1000 : 0
		// Electron has a bug on linux where it
		// won't initialize properly when using
		// transparency. To work around that, it
		// is necessary to delay the window
		// spawn function.
	);
});

const activeApp = () => {
	const script = `
	tell application "System Events"
		set activeApp to displayed name of first application process whose frontmost is true

		set searchedApps to a reference to ((every application process) whose name contains activeApp or displayed name contains activeApp)
		if (count of searchedApps) is 1 then
			set appPath to POSIX path of application file of searchedApps as text
		else if (count of searchedApps) > 1 then
			set appPath to POSIX path of application file of searchedApps
			set appPath to item 1 of appPath
		end if

		return {activeApp, appPath}
	end tell	
	`;
	return new Promise((resolve, reject) => {
		applescript.execString(script, (err, rtn) => {
			if (err)
				reject(err);
			resolve(rtn);
		});
	});
}

function spawnWindow(){
	let show = false;
	const {screen} = require("electron");
	const display = screen.getPrimaryDisplay()
	const displaySize = display.size;
	const scale = display.scaleFactor;
	win = new glasstron.BrowserWindow({
		width: displaySize.width,
		height: 400,
		frame: false,
		resizable: false,
		movable: false,
		minimizable: false,
		enableLargerThanScreen: true,
		maximizable: false,
		visualEffectState: "active",
		vibrancy: "fullscreen-ui"
	});
	win.blurType = "acrylic";
	app.dock.hide();
	win.setAlwaysOnTop(true, "screen-saver", 1);
	win.setFullScreenable(false);
	win.setVisibleOnAllWorkspaces(true, {visibleOnFullScreen: true});
	win.setPosition(0, displaySize.height);
	win.setBlur(true);


	globalShortcut.register('Command+B', () => {
		if (show) {
			win.setPosition(0, displaySize.height, true);
			show = false;
		}
		else {
			win.setPosition(0, displaySize.height - 400, true);
			win.focus();
			show = true;
		}
	})
	win.toggleDevTools();
	win.loadURL(`file://${__dirname}/views/index.html`);

	win.on("blur", () => {
		console.log("blur");
		win.setPosition(0, displaySize.height, true);
		show = false;
	});
	clipboard
	.on('text-changed', async () => {
		let currentText      = clipboard.readText();
		const [app, appPath] = await activeApp();
		const plistPath = `${appPath}/Contents/Info.plist`;
		const item           = new ClipboardItem("text", currentText, app);
		const iconName = plist.parse(fs.readFileSync(plistPath, "utf8")).CFBundleIconFile;
		const appendix = iconName.match(/.icns$/) ? "" : ".icns";
		const icon = `${appPath}/Contents/Resources/${iconName}${appendix}`;
		iconutil.toIconset(icon, function(err, icons) {
			console.log(icons);
			const i = icons["icon_128x128@2x.png"];
			fs.writeFileSync(`./icons/${app}.png`, i);
		});
		console.log(icon);
	})
	.on('image-changed', () => {
		let currentIMage = clipboard.readImage()
		console.log(currentIMage);
	})
	.startWatching();

	return win;
}


app.on('will-quit', () => {
	// Unregister all shortcuts.
	globalShortcut.unregisterAll()
  })